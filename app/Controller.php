<?php

namespace app;

/**
 * Description of Controller
 *
 * @author Gaetane le Grange <gaetane@rodel.co.za>
 */
class Controller {

    protected $f3;
    protected $template;
    protected $cache;
    protected $page_title;
    protected $page_description;
    protected $page_template;
    /*
     *
     */
    private $page_classes = array();
    private $page_scripts = array(
        'header' => array(),
        'footer' => array(),
    );
    private $page_styles  = array(
        'header' => array(),
        'footer' => array(),
    );



    public function __construct() {
        $this->f3       = \Base::instance();
        $this->template = \Template::instance();
        $this->cache    = \Cache::instance();
    }



    public function beforeroute() {
        $this->set_page_styles('header', '1', 'bootswatch', '/node_modules/bootswatch/dist/quartz/bootstrap.css');
        $this->set_page_styles('header', '2', 'template', '/ui/laf/template.css');
        $this->set_page_styles('header', '3', 'fontawesome', '/node_modules/@fortawesome/fontawesome-free/css/all.css');

        $this->set_page_scripts('footer', '1', 'bootstrap', '/node_modules/bootstrap/dist/js/bootstrap.bundle.js');
        $this->set_page_scripts('footer', '3', 'main', '/ui/js/main.js');
    }



    public function afterroute() {

        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Pragma: no-cache');

        $this->f3->set('PAGE', array(
            'base_url'    => 'https://' . $_SERVER['HTTP_HOST'],
            'title'       => $this->page_title,
            'description' => $this->page_description,
            'template'    => $this->page_template,
            'body'        => array(
                'class' => $this->page_classes,
            ),
            'styles'      => $this->render('styles'),
            'scripts'     => $this->render('scripts'),
        ));

        echo $this->template->render('/ui/layout.php');
    }



    protected function set_page_class($class) {
        if (!is_array($class) && !\is_string($class)) {
            return false;
        }
        if (\is_string($class) && !is_array($class)) {
            $class = array($class);
        }
        $this->page_classes = array_merge($this->page_classes, $class);
        $this->page_classes = \array_unique($this->page_classes);
    }



    protected function set_page_scripts($position, $order, $identifier, $path) {
        $this->page_scripts[$position][$order][$identifier] = $path;
        \ksort($this->page_scripts[$position]);
    }



    protected function set_page_styles($position, $order, $identifier, $path) {
        $this->page_styles[$position][$order][$identifier] = $path;
        \ksort($this->page_styles[$position]);
    }



    private function render($type) {

        $version = 2;

        switch ($type) {
            case 'styles':
                $output = array(
                    'header' => array(),
                    'footer' => array(),
                );
                foreach ($this->page_styles as $position => $pinfo) {
                    foreach ($pinfo as $order => $oinfo) {
                        foreach ($oinfo as $identifier => $iinfo) {
                            $output[$position][] = '<link href="' . $iinfo . '?v=' . $version . '" rel="stylesheet" type="text/css" />';
                        }
                    }
                }
                break;
            case 'scripts':
                $output = array(
                    'header' => array(),
                    'footer' => array(),
                );
                foreach ($this->page_scripts as $position => $pinfo) {
                    foreach ($pinfo as $order => $oinfo) {
                        foreach ($oinfo as $identifier => $iinfo) {
                            $output[$position][] = '<script src="' . $iinfo . '?v=' . $version . '" type="text/javascript"></script>';
                        }
                    }
                }
                break;
            default:
                break;
        }

        return $output;
    }



    public function curl($options, $ttl) {

        if (defined('DEV')) {
            $options[\CURLOPT_SSL_VERIFYHOST] = 0;
            $options[\CURLOPT_SSL_VERIFYPEER] = 0;
        }

        $serial = \md5(json_encode($options));

        if (CACHE_CURL === false) {
            $this->cache->clear($serial);
        }
        elseif ($this->cache->exists($serial, $output)) {
            return $output;
        }

        $ch = \curl_init();
        \curl_setopt_array($ch, $options);
        $output = \curl_exec($ch);

        if ($output === false) {
            $output = curl_error($ch);
        }
        else {
            if (CACHE_CURL === true) {
                $this->cache->set($serial, $output, $ttl);
            }
        }
        \curl_close($ch);

        return $output;
    }

}


