<?php

namespace app;



/**
 * Description of Model
 *
 * @author Gaetane le Grange <gaetane@rodel.co.za>
 */
class Model {

    protected $f3;
    protected $controller;



    public static function create(\app\Controller $controller) {
        $class = \get_called_class();
        return new $class($controller);
    }



    public function __construct(\app\Controller $controller) {
        $this->controller = $controller;
        $this->f3         = \Base::instance();
    }

}


