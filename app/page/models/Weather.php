<?php

namespace app\page\models;

/**
 * @author Gaetane le Grange <gaetane@rodel.co.za>
 */
class Weather extends \app\Model {

    private $output  = array();


    public function get_weather() {

        $result = $this->controller->curl(
                array(
                    \CURLOPT_USERAGENT      => 'rodel.co.za gaetane@rodel.co.za',
                    \CURLOPT_URL            => 'https://api.met.no/weatherapi/locationforecast/2.0/complete?lat=-29.82433&lon=31.0085',
                    \CURLOPT_RETURNTRANSFER => true,
                ), 60 * 60);
        $this->output = json_decode($result);

        return $this->output;
    }

}


