<?php

namespace app\page;

class PageController extends \app\Controller {



    public function home() {
        $this->page_title       = 'Hello World';
        $this->page_description = 'Simplicity is the soul of efficiency.';
        $this->page_template    = '/app/page/views/page_home.php';
        $this->set_page_class(array('home'));
    }



    public function error_page() {
        $number = $this->f3->get('PARAMS.number');
        $this->f3->error($number);
    }

}