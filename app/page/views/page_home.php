<section>
    <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2" class=""></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3" class="" aria-current="true"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active" id="carousel_slide_1">
                <div class="container">
                    <div class="carousel-caption text-start">
                        <h1 class="text-white">What's the weather?</h1>
                        <p>Create a page to show the weather conditions in Morningside, Durban - home of Rodel!</p>
                        <p>
                            <a class="btn btn-lg btn-primary" href="/your/link/here">
                                <i class="fas fa-umbrella"></i> Will I need an umbrella?
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="carousel-item" id="carousel_slide_2">
                <div class="container">
                    <div class="carousel-caption highlight text-center">
                        <h1 class="text-dark">Uh oh...</h1>
                        <p class="text-dark">Handle errors gracefully. You know how upset users get when something goes wrong. Maybe play some soothing music.
                            Or tell a joke. Users love a good joke on an error page. Calms them right down. Lol.</p>
                        <p>
                            <a class="btn btn-lg btn-primary" href="{{ 'error_page', 'number=403' | alias }}">
                                <i class="fas fa-times-circle"></i> 403
                            </a>
                            <a class="btn btn-lg btn-primary" href="{{ 'error_page', 'number=404' | alias }}">
                                <i class="fas fa-search-location"></i> 404
                            </a>
                            <a class="btn btn-lg btn-primary" href="{{ 'error_page', 'number=500' | alias }}">
                                <i class="fas fa-dizzy"></i> 500
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="carousel-item" id="carousel_slide_3">
                <div class="container">
                    <div class="carousel-caption text-end">
                        <h1 class="text-white">Show Off</h1>
                        <p>Build a page that does whatever you like. But keep it simple ;-)</p>
                        <p><a class="btn btn-lg btn-primary" href="/your/link/here">What you got?</a></p>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</section>

<section class="container marketing">

    <div class="row">
        <div class="col-lg-4">
            <i class="fas fa-hourglass-half fa-4x"></i>
            <h2>Why so slow?</h2>
            <p>Site seem a bit heavy to you? Maybe some tweaking is in order.</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <i class="fas fa-flushed fa-4x"></i>
            <h2>Future so bright!</h2>
            <p>And so is this theme. Change the default theme to something more appropriate.</p>
            <p><a class="btn btn-secondary" href="https://bootswatch.com/" target="_blank">Bootswatch »</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <i class="fas fa-users-cog fa-4x"></i>
            <h2>That said...</h2>
            <p>Users do like to customise. A javascript based theme switcher might be nice.</p>
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
</section>

<hr class="featurette-divider">

<section class="container">
    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Keep it <span class="text-muted">simple.</span></h2>
            <p class="lead">We aren't wanting you to spend days creating elaborate solutions.</p>
        </div>
        <div class="col-md-5">
            <svg class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 500x500" preserveAspectRatio="xMidYMid slice" focusable="false">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#eee"></rect>
                <text x="50%" y="50%" fill="#aaa" dy=".3em">500x500</text>
            </svg>

        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">Make sure to review all the files <span class="text-muted">before you start.</span></h2>
            <p class="lead">Most of what you need is already included :-)</p>
        </div>
        <div class="col-md-5 order-md-1">
            <svg class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 500x500" preserveAspectRatio="xMidYMid slice" focusable="false">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#eee"></rect>
                <text x="50%" y="50%" fill="#aaa" dy=".3em">500x500</text>
            </svg>

        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Do what you think you need to do, <span class="text-muted">not what you think we want.</span></h2>
            <p class="lead">Trust your instincts and experience. Think you can do it better? Then you absolutely should ;-)</p>
        </div>
        <div class="col-md-5">
            <svg class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 500x500" preserveAspectRatio="xMidYMid slice" focusable="false">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#eee"></rect>
                <text x="50%" y="50%" fill="#aaa" dy=".3em">500x500</text>
            </svg>

        </div>
    </div>

    <hr class="featurette-divider">


</section><!-- /.container -->

