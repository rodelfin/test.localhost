<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>{{@PAGE.title}}</title>
    <meta name="description" content="{{@PAGE.description}}"/>

    {{implode(PHP_EOL, @PAGE.styles.header) | raw}} {{implode(PHP_EOL, @PAGE.scripts.header) | raw}}
</head>

<body class="{{implode(' ', @PAGE.body.class) | raw}}">

<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Hello World</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ 'home' | alias }}">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<check if="!empty(@PAGE.template)">
    <main>

            <include href="{{@PAGE.template}}"/>
        </section>
    </main>
</check>

<footer>
    {{(is_array(@PAGE.styles.footer) ? implode(PHP_EOL, @PAGE.styles.footer) : '') | raw}} {{(is_array(@PAGE.scripts.footer) ? implode(PHP_EOL,
    @PAGE.scripts.footer) : '') | raw}}

    <section class="container">
        <p class="float-end"><a href="#">Back to top</a></p>
        <p><a href="https://rodel.co.za" target="_blank">© 2000–{{date('Y')}} Rodel Financial Services (PTY) LTD</a></p>
    </section>
</footer>
</body>
</html>
