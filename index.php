<?php

require 'vendor/autoload.php';
require 'app/Configuration.php';

/*
 * https://fatfreeframework.com/3.7/home
 */
$f3 = \Base::instance();

$f3->set('CACHE', 'folder=' . $_SERVER['DOCUMENT_ROOT'] . '/tmp/');

/*
 * Routes
 */
$f3->route('GET @home: /', '\app\page\PageController->home');
$f3->route('GET @error_page: /error/@number', '\app\page\PageController->error_page');

/*
 * Error
 */
$f3->set('ONERROR', function($f3) {
    var_dump($f3->get('ERROR'));
   echo 'What should we do if there is an error?';
});

$f3->run();